import requests


class RequestUtils:

    def __init__(self):
        pass

    def send_request(self,method,url,data=None,headers=None,content_type=None,params=None,files=None):
        try:
            if method == 'get':
                res = requests.get(url=url,params=data,headers=headers).json()
                return res
            elif method == 'post':
                if content_type == 'application/json':
                    res = requests.post(url=url,json=data,params=params,headers=headers).json()
                    return res
                elif content_type is None:
                    res = requests.post(url=url,data=data,params=params,headers=headers).json()
                    return res
                else:
                    headers.pop('Content-Type')
                    res = requests.post(url=url,files=data,params=params,headers=headers).json()
                    return res
            elif method == 'put':
                if content_type == 'application/json':
                    res = requests.put(url=url,json=data,params=params,headers=headers).json()
                    return res
                else:
                    res = requests.put(url=url,data=data,params=params,headers=headers).json()
                    return res
            elif method == 'delete':
                res = requests.delete(url=url).json()
                return res
            else:
                print("method not allowed")
        except Exception as e:
            print("http请求错误：{0}".format(e))


if __name__ == "__main__":
    url = 'https://api.xdclass.net/pub/api/v1/web/all_category'
    method = 'get'
    r = RequestUtils()
    res = r.send_request(method=method,url=url)
    print(res)


    url2 = 'https://api.xdclass.net/pub/api/v1/web/web_login'
    method2 = 'post'
    data = {'phone':'15618300212','pwd':'shuoye666'}
    res2 = r.send_request(method=method2,url=url2,data=data)
    print(res2)