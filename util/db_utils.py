import pymysql
from warnings import filterwarnings
from config import handle_ini

filterwarnings('ignore', category=pymysql.Warning)


class MysqlDb:

    # 初始化实例对象
    def __init__(self, db_section, db_name):
        # 根据section和dbname，取到对应数据库的配置字段值
        self.handle = handle_ini.HandleIni()
        self.host = str(self.handle.getIni(db_section, 'host'))
        self.user = str(self.handle.getIni(db_section, 'user'))
        self.pwd = str(self.handle.getIni(db_section, 'pwd'))
        self.db = str(self.handle.getIni(db_section, db_name))

        # 连接数据库
        self.conn = pymysql.connect(host=self.host,port=3306,user=self.user,password=self.pwd,db=self.db,use_unicode=True,charset='utf8')
        # 使用cursor方法操作游标，得到一个可以执行sql语句，并且操作结果作为字典返回的游标
        self.cur = self.conn.cursor(cursor=pymysql.cursors.DictCursor)

    # __del__()方法，功能正好和__init__()相反，其用来销毁实例化对象，释放内存
    def __del__(self):
        self.cur.close()
        self.conn.close()

    def query(self,sql,state='all'):
        '''
        查询
        :param sql:
        :param state:all是默认查询全部
        :return:查询结果
        '''
        self.cur.execute(sql)
        if state == 'all':
            data = self.cur.fetchall()
        else:
            data = self.cur.fetchone()
        return data

    def execute(self,sql):
        try:
            rows = self.cur.execute(sql)
            self.conn.commit()
            return rows
        except Exception as e:
            print("数据库操作异常 {0}".format(e))
            self.conn.rollback()


if __name__ == '__main__':
    msql = MysqlDb('mysql_ddmc_api_test',"db_name")
    sql = 'select * from `case` '
    data = msql.query(sql=sql,state='all')
    print(data)


    mysql2 = MysqlDb('srzp_test_env','api_test')
    sql2 = "select * from `testcases`"

    data2 = mysql2.query(sql2)
    print(data2)