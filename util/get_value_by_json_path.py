from jsonpath_rw import parse
import re


def GetValueByJsonPath(response,json_path):
    json_path = parse(json_path)
    # 通过jsonpath去匹配目标字段，如果匹配不成功，则返回None
    try:
        madle = json_path.find(response)
        return [math.value for math in madle][0]
    except Exception as e:
        print(e)
        return None

def GetValueByRegExp(response,reg_exp):
    nameRegex = re.compile(reg_exp)
    mo = nameRegex.search(str(response))
    value = mo.group(1)
    return value

# def test_get_value_by_json_path():
#     json_path = "data.token"
#     res = {
#     "code": 0,
#     "data": {
#         "head_img": "https://xd-video-pc-img.oss-cn-beijing.aliyuncs.com/xdclass_pro/default/head_img/1.jpeg",
#         "name": "飞塔",
#         "token": "xdclasseyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ4ZGNsYXNzIiwicm9sZXMiOiIxIiwiaW1nIjoiaHR0cHM6Ly94ZC12aWRlby1wYy1pbWcub3NzLWNuLWJlaWppbmcuYWxpeXVuY3MuY29tL3hkY2xhc3NfcHJvL2RlZmF1bHQvaGVhZF9pbWcvMS5qcGVnIiwiaWQiOjEyMDU0LCJuYW1lIjoi6aOe5aGUIiwiaWF0IjoxNTk2MTE4NzE3LCJleHAiOjE1OTY3MjM1MTd9.W7wg3DYWt27YOQyLyFw2v8KcpFedwqX8f8_QcNv98Sw"
#     },
#     "msg": 1
# }
#     val = GetValueByJsonPath(response=res,json_path=json_path)
#     print(val)
#
# test_get_value_by_json_path()

