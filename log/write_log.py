import os
import datetime


class WriteLog:

    def __init__(self):
        self.file = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + 'log.txt'

    def wri_log(self,log):
        log_path = os.path.join(os.path.dirname(__file__), self.file)
        with open(file=log_path,mode="a+",encoding='utf-8') as f:
            f.write(log+"\n")
