# coding = utf-8
from operator import methodcaller
from util.db_utils import MysqlDb
import datetime
import time
import json
from util.request_utils import RequestUtils
from util.send_mail import SendMail
from util import get_value_by_json_path
from . import request_body_functions
from . import params_functions

from pprint import pprint


class RunTestCases:

    def __init__(self):
        self.my_db = MysqlDb('srzp_test_env', "db_name")
        # self.empty_relied_fields_values()
        self.run = RequestUtils()

    def loadAllSingleCaseByApp(self, app, module=None):
        '''
        根据App加载全部测试用例
        :param app:
        :return:
        '''
        if module is not None:
            sql = "select * from `testcases` where app='{0}' and module='{1}' and (test_suite_name IS NULL OR test_suite_name = '') and run='yes'".format(app,module)
        else:
            sql = "select * from `testcases` where app='{0}' and (test_suite_name IS NULL OR test_suite_name = '') and run='yes'".format(app)
        res = self.my_db.query(sql)
        return res

    def loadAllTestSuitesNamesByApp(self, app, module=None):
        '''
        根据App加载全部测试用例
        :param app:
        :return:
        '''
        if module is not None:
            sql = "select DISTINCT(test_suite_name) from `testcases` where app='{0}' and module='{1}' and (test_suite_name IS NOT NULL AND test_suite_name !='') and run='yes'".format(app, module)
        else:
            sql = "select DISTINCT(test_suite_name) from `testcases` where app='{0}' and (test_suite_name IS NOT NULL AND test_suite_name !='') and run='yes'".format(app)
        res = self.my_db.query(sql)
        return res

    def findCaseById(self, case_id):
        '''
        根据id找到测试用例
        :param case_id:
        :return:
        '''
        sql = "select * from `testcases` where case_id='{0}'".format(case_id)
        res = self.my_db.query(sql, state='one')
        return res

    def loadConfigByAppAndKey(self, app, key):
        '''
        根据App和Key获取对应配置
        :param app:
        :param key:
        :return:
        '''
        sql = "select * from `config` where app='{0}' and dict_key='{1}' ".format(app, key)
        res = self.my_db.query(sql, state='one')
        return res

    def updateResultByCaseId(self, response, is_pass, msg, case_primary_id):
        '''
        根据测试用例主键id，更新响应内容和测试内容
        :param response:
        :param is_pass:
        :param msg:
        :param case_primary_id:
        :return:
        '''
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if is_pass:
            sql = " update `testcases` set response='{0}',is_pass='{1}',msg='{2}',update_time='{3}' " \
                  "where id={4}".format("", is_pass, msg, current_time, case_primary_id)
        else:
            sql = "update `testcases` set response=\"{0}\",is_pass=\"{1}\",msg=\"{2}\",update_time='{3}' " \
                  "where id={4}".format(str(response), is_pass, msg, current_time, case_primary_id)
        rows = self.my_db.execute(sql)
        return rows

    def runAllCase(self, app, module=None):
        '''
        根据App（应用）执行全部测试用例
        :param app:
        :return:
        '''
        # 获取接口域名
        api_hosts_obj = self.loadConfigByAppAndKey(app, 'hosts')
        # print(api_hosts_obj)
        # 获取全部用例
        single_cases = self.loadAllSingleCaseByApp(app, module)

        # 遍历全部用例
        for case in single_cases:
                try:
                    # 执行用例
                    response = self.runCase(case=case, api_hosts_obj=api_hosts_obj)
                    # 断言判断
                    assert_msg = self.assertResponse(case=case, response=response)

                    # 更新结果存储数据库
                    self.updateResultByCaseId(response=response, is_pass=assert_msg['is_pass'], msg=assert_msg['msg'],
                                              case_primary_id=case['id'])
                    # print("更新结果rows={0}".format(str(rows)))
                except Exception as e:
                    print("用例id={0},标题：'{1}',执行报错'{2}'".format(case['id'], case['api_title'], e))

        test_suite_names = self.loadAllTestSuitesNamesByApp(app, module)
        for items in test_suite_names:
            test_suite_name = items['test_suite_name']
            sql = "select * from `testcases` where app='{0}' and test_suite_name='{1}' and run='yes' ORDER BY sort_id".format(
                app, test_suite_name)
            test_suite_cases = self.my_db.query(sql)
            # 遍历全部用例
            for case in test_suite_cases:
                    try:
                        # 执行用例
                        response = self.runCase(case=case, api_hosts_obj=api_hosts_obj)
                        # 断言判断
                        assert_msg = self.assertResponse(case=case, response=response)

                        # 更新结果存储数据库
                        self.updateResultByCaseId(response=response, is_pass=assert_msg['is_pass'], msg=assert_msg['msg'],
                                                  case_primary_id=case['id'])
                        # print("更新结果rows={0}".format(str(rows)))
                    except Exception as e:
                        print("用例id={0},标题：'{1}',执行报错'{2}'".format(case['id'], case['api_title'], e))

        # 发送测试报告
        self.sendTestReport(app, module)

    def runCase(self, case, api_hosts_obj):
        '''
        执行单个用例
        :param case:
        :param api_host_obj:
        :return:
        '''
        api_hosts_obj_values = eval(api_hosts_obj['dict_value'])
        case_host_value = case['server']
        headers = json.loads(case['headers'])

        # 初始化content_type为None，如果有Content_type，则赋值给content_type
        content_type = None
        if 'Content-Type' in headers:
            content_type = headers['Content-Type']

        req_url = api_hosts_obj_values[case_host_value] + case['url']
        req_method = case['method']

        params = {}
        if case['params']:
            params = json.loads(case['params'])

        if case['params_func']:
            params_func_name = case['params_func']
            params = params.copy()
            func_params = methodcaller(params_func_name)(params_functions.ParamsFunctions())
            params.update(func_params)

        req_body = {}
        if case['request_body']:
            req_body = json.loads(case['request_body'])

        if case['request_body_func']:
            function_name = case['request_body_func']
            if 'file' not in case['request_body_func']:
                req_body = req_body.copy()
                func_req_body = methodcaller(function_name)(request_body_functions.RequestBodyFunctions())
                req_body.update(func_req_body)
            else:
                req_body = methodcaller(function_name)(request_body_functions.RequestBodyFunctions())


        # 判断是否有前置条件
        if case['pre_fields']:
            pre_fields = eval(case['pre_fields'])
            for pre_case_id in pre_fields:
                pre_case = self.findCaseById(pre_case_id)

                # 前置条件不通过则返回
                if pre_case['is_pass'] == 'False':

                    # 获取到前置条件的response,并转换成dict格式
                    pre_response = eval(pre_case['response'])
                    pre_response['msg'] = "前置条件不通过," + pre_response['msg']
                    return pre_response

                # 判断需要的是前置条件的哪个字段
                all_pre_fields = eval(case['pre_fields'])
                pre_fields = all_pre_fields[str(pre_case_id)]

                for pre_field in pre_fields:
                    field_name = pre_field['field']
                    # 基于获取到的所需要的字段名，在前置条件case中的relied_fields_values中去查找
                    relied_fields_values = eval(pre_case['relied_fields_values'])

                    if field_name in relied_fields_values and relied_fields_values[field_name] is not None:
                        field_value = relied_fields_values[field_name]

                        # 判断是放在url里还是放在body里

                        if pre_field['scope'] == 'headers':
                            # 从url中,寻找同名的字段，替换对应的值
                            for k,v in headers.items():
                                # print(k,v)
                                if v == field_name:
                                    headers[k] = field_value
                        elif pre_field['scope'] == 'body':
                            for k,v in req_body.items():
                                # 兼容array的参数提取，这块可以优化
                                array_value = []
                                array_value.append(field_value)
                                array_name = []
                                array_name.append(field_name)
                                if v == field_name:
                                    req_body[k] = field_value
                                elif v == array_name:
                                    req_body[k] = array_value
                        elif pre_fields['scope'] == 'params':
                            pass

                    # 前置用例通过，参数提取失败的容错处理。
                    else:
                        # 如果用例通过，则将pre_response初始化成字典
                        pre_response = {}
                        pre_response['msg'] = "前置条件参数提取失败"
                        return pre_response
        # print(headers)
        # print(json.dumps(req_body))
        # pprint(headers)

        res = self.run.send_request(method=req_method, url=req_url, headers=headers, data=req_body,
                                    params=params, content_type=content_type)

        # 判断是否需要提取被其他接口依赖的数据，如果有被其他接口依赖的数据，则将这些被依赖的数据写入数据库
        if case['relied_fields']:
            self.write_relied_fields_values(case,response=res)

        # 执行后置sql
        if case['tear_down_sql']:
            # print(case['tear_down_sql'])
            self.my_db.execute(case['tear_down_sql'])

        pprint(res)
        return res

    def empty_relied_fields_values(self):
        sql = " update `testcases` set relied_fields_values=\"{0}\"".format("{}")
        rows = self.my_db.execute(sql)
        return rows

    def write_relied_fields_values(self,case,response):
        relied_fields = json.loads(case['relied_fields'])
        relied_fields_values = {}

        for relied_field in relied_fields:
            field_json_path = relied_field['field_json_path']
            field_value = get_value_by_json_path.GetValueByJsonPath(response, field_json_path)
            field_name = relied_field['field_name']
            relied_fields_values[field_name] = field_value
        sql = " update `testcases` set relied_fields_values=\"{0}\" where id={1}".format(str(relied_fields_values),case['id'])
        rows = self.my_db.execute(sql)
        return rows


    def assertResponse(self, case, response):
        '''
        断言响应内容，更新用例执行情况。{"is_pass":True,"mas":"code is wrong"}
        :param case:
        :param response:
        :return:
        '''
        assert_type = case['assert_type']
        expect_result = case['expect_result']

        is_pass = False
        msg = ""

        # 通过try捕获异常，如前置条件通过，参数提取失败的异常
        try:
            # 判断业务状态码
            if assert_type == 'code':
                response_code = response['code']
                if int(expect_result) == int(response_code):
                    is_pass = True
                    # print("测试通过")
                else:
                    is_pass = False
                    # print("测试用例不通过")
            # 判断data对象中数组的长度
            elif assert_type == 'data_json_rows':
                data_rows = None
                # 兼容接口规范
                if 'data' in response:
                    data_rows = response['data']['rows']
                elif 'datas' in response:
                    data_rows = response['datas']['rows']
                if data_rows is not None and isinstance(data_rows, list) and len(data_rows) > int(expect_result):
                    is_pass = True
                    # print("测试通过")
                else:
                    is_pass = False
                    # print("测试不通过")
            # 判断数组的长度
            elif assert_type == 'data_json_array':
                data_array = None
                # 兼容接口规范
                if 'data' in response:
                    data_array = response['data']
                elif 'datas' in response:
                    data_array = response['datas']
                if data_array is not None and isinstance(data_array, list) and len(data_array) > int(expect_result):
                    is_pass = True
                    # print("测试通过")
                else:
                    is_pass = False
                    # print("测试不通过")
            elif assert_type == 'data_json':
                data = None
                # 兼容接口规范
                if 'data' in response:
                    data = response['data']
                elif 'datas' in response:
                    data  = response['datas']
                if data is not None and isinstance(data, dict) and len(data) > int(expect_result):
                    is_pass = True
                    # print("测试通过")
                else:
                    is_pass = False
                    # print("测试不通过")
            elif assert_type == 'fields_exist':
                expect_result = eval(expect_result)
                for item in expect_result:
                    if item in str(response):
                        is_pass = True
                    else:
                        is_pass = False
            if 'msg' in response:
                msg = "模块:{0},标题:{1},断言类型:{2},msg:{3}".format(case['module'], case['api_title'], assert_type, response['msg'])
            elif 'message' in response:
                msg = "模块:{0},标题:{1},断言类型:{2},message:{3}".format(case['module'], case['api_title'], assert_type,
                                                              response['message'])
        except Exception as e:
            print(e)
            msg = response

        # 拼装信息
        assert_msg = {"is_pass": is_pass, "msg": msg}
        return assert_msg

    def sendTestReport(self, app, module=None):
        '''
        发送邮件测试报告
        :param app:
        :return:
        '''
        # 加载全部测试用例
        if module is not None:
            results = self.loadAllSingleCaseByApp(app,module)
            test_suite_names = self.loadAllTestSuitesNamesByApp(app,module)
        else:
            results = self.loadAllSingleCaseByApp(app)
            test_suite_names = self.loadAllTestSuitesNamesByApp(app)

        title = app + "接口自动化测试报告"
        content = """
        <html><body>
            <h4>{0} 接口测试报告：</h4>
            <h4> 此次一共运行{1}个用例，其中通过{2}个用例，不通过{3}个用例，通过率为{4}，未通过率为{5}</h4>
            <h3>测试不通过的用例</h3>
            <table border="1">
            <tr>
              <th>编号</th>
              <th>模块</th>
              <th>用例集标题</th>
              <th>接口名称</th>
              <th>备注</th>
              <th>响应</th>
            </tr>
            {6}
            </table>
            <h3>测试通过的用例</h3>
            <table border="1">
            <tr>
              <th>编号</th>
              <th>模块</th>
              <th>用例集标题</th>
              <th>接口名称</th>
              <th>备注</th>
              <th>响应</th>
            </tr>
            {7}
            </table></body></html>
        """
        failed_cases_template = ""
        passed_cases_template = ""
        passed_cases_num = 0
        failed_cases_num = 0

        for case in results:
            if case['is_pass'] == 'False':
                failed_cases_template += "<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>".format(
                    case['id'], case['module'], case['test_suite_name'], case['api_title'], case['msg'], case['response']
                )
                failed_cases_num +=1
            elif case['is_pass'] == 'True':
                passed_cases_template += "<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>".format(
                    case['id'], case['module'], case['test_suite_name'], case['api_title'], case['msg'], case['response']
                )
                passed_cases_num +=1

        for items in test_suite_names:
            test_suite_name = items['test_suite_name']
            sql = "select * from `testcases` where app='{0}' and test_suite_name='{1}' and run='yes' ORDER BY sort_id".format(
                app,test_suite_name)
            test_suite_cases = self.my_db.query(sql)
            for case in test_suite_cases:
                if case['is_pass'] == 'False':
                    failed_cases_template += "<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>".format(
                        case['id'], case['module'], case['test_suite_name'], case['api_title'], case['msg'], case['response']
                    )
                    failed_cases_num +=1
                elif case['is_pass'] == 'True':
                    passed_cases_template += "<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>".format(
                        case['id'], case['module'], case['test_suite_name'], case['api_title'], case['msg'], case['response']
                    )
                    passed_cases_num +=1

        total_cases_num = passed_cases_num + failed_cases_num
        pass_rate = '{:.2%}'.format(passed_cases_num/float(total_cases_num))
        fail_rate = '{:.2%}'.format(failed_cases_num / float(total_cases_num))


        current_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        content = content.format(current_time, total_cases_num,passed_cases_num,failed_cases_num,pass_rate,fail_rate,failed_cases_template,passed_cases_template)
        mail_host = self.loadConfigByAppAndKey(app, "mail_host")['dict_value']
        mail_sender = self.loadConfigByAppAndKey(app, "mail_sender")['dict_value']
        mail_auth_code = self.loadConfigByAppAndKey(app, "mail_auth_code")['dict_value']
        mail_receivers = self.loadConfigByAppAndKey(app, "mail_receivers")['dict_value'].split(",")
        mail = SendMail(mail_host)
        mail.send(title, content, mail_sender, mail_auth_code, mail_receivers)


if __name__ == '__main__':
    test = RunTestCases()
    test.runAllCase('熟仁直聘st3')
    # test.runAllCase('熟仁直聘st3','login')