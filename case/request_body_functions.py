import os
import datetime
import time
import random
# from util.db_utils import MysqlDb
# import requests


class RequestBodyFunctions:

    def __init__(self):
        self.file_dir = os.path.dirname(os.path.abspath(__file__))
        # self.my_db = MysqlDb('ddmc_test_env',"testcases_name")

    # def findCookieById(self, cookie_case_id):
    #     '''
    #     根据id找到测试用例
    #     :param cookie_case_id:
    #     :return:
    #     '''
    #     sql = "select * from `testcases_manager_cookies` where case_id='{0}'".format(cookie_case_id)
    #     res = self.my_db.query(sql, state='one')
    #     return res

    def sleep_two_second(self):
        body = {}
        time.sleep(2)
        return body


    def headshot_file(self):
        file_name = os.path.join(self.file_dir,"files/qiqi.jpeg")
        file = {'file': open(file_name, 'rb')}
        return file


    def create_new_store(self):
        body = {
            "store_id":"",
            "sort":"",
            "update_time":"",
            "address":"盛夏路600号",
            "phone":"18888888888",
            "remark":"",
            "is_purchase":"",
            "is_sale":"",
            "is_scan":"",
            "city_id":"5c413ca16c70ba503c8bd74f",
            "region_id":"32",
            "general_region_id":"",
            "type":"3",
            "is_chive_notice":"",
            "is_test":"",
            "storage_type":"",
            "is_tob":"",
            "is_aquatic":"",
            "start_date":"",
            "end_date":""
        }
        created_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        body['store_name'] = "测试小仓" + created_time
        return body



if __name__ == "__main__":
    r = RequestBodyFunctions()
    data = r.adjust_qty_to_one()
    print(data)
