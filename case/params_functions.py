import os
import datetime
import time
import random
from util.db_utils import MysqlDb
import requests


class ParamsFunctions:

    def __init__(self):
        pass
        # self.file_dir = os.path.dirname(os.path.abspath(__file__))
        # self.my_db = MysqlDb('ddmc_test_env',"testcases_name")

    def get_timestamp(self):
        time.sleep(1.5)
        params = {}
        params['_'] = int(time.time()*1000)
        return params

    def get_timestamp_sleepeighten(self):
        time.sleep(18)
        params = {}
        params['_'] = int(time.time()*1000)
        return params



if __name__ == "__main__":
    p = ParamsFunctions()
    params = p.get_timestamp()
    print(params)
