import configparser
import os


class HandleIni:

    def loadIni(self):
        # 本地环境路径
        file_path = os.path.join(os.path.dirname(__file__), "server.ini")
        cf = configparser.ConfigParser()
        cf.read(file_path, encoding='utf-8-sig')
        return cf

    def getIni(self, section, key):
        cf = self.loadIni()
        data = cf.get(section, key)
        return data


# def test_handle_ini():
#     hi = HandleIni()
#     base_url = hi.getIni('server','username')
#     print(base_url)
#
# test_handle_ini()